# Author: Michael Hale
# CSCI 331 Spring 2013
# Homework 2
# Reference: http://www.cs.unca.edu/brock/classes/Spring2013/csci331/notes/networkingserver.html

import re
import socket

def serverCAS():
    pattern = re.compile('^SWAP\s(\S+)\sUSING\s(\S+)\n$')

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1);
        sock.bind(('', 33101))
        sock.listen(1)

        while True:
            strm, addr = sock.accept()
            line = strm.recv(1000)
            while line:
                print line
                match = pattern.match(line)
                if (match == None):
                   repl = 'Bad Syntax'
                else:
                   repl = ' '.join(['RETURN', match.group(1), 'USING', match.group(2)])
                strm.send(repl+'\n\r')
                line = strm.recv(1000)
            strm.close()
    except KeyboardInterrupt: 
        print 'Exit'
        sock.close()

if __name__ == "__main__":
    serverCAS()
